#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
#-------------------------------------------------------------------------------
# Name:     loadseries.py
# Author:   Radomirs Cirskis
#
# Created:  2016-08-17
# Licence:  WTFPL
#-------------------------------------------------------------------------------
"""
Created on Wed Aug 17 19:03:43 2016
@author: nad2000

INPUT:
    Sabre Itinerary XML (no name space)
    MySQL DB connection parameters (or via config.ini)

OUTPU:
    New or updated record in DB

STEPS"
    1. parse XML file;
    ....

PREREQUISITES:
    Python 3.x (python3 Ubuntu package)
    MySQL-python (python3-mysql.connector Ubuntu package)
    Requests (python3-request Ubuntu package)
"""

from __future__ import print_function 

import xml.etree.ElementTree as ET
import MySQLdb as DB
import re
import sys, glob
import os, os.path
import argparse
from datetime import datetime, date, time

# Constants:
VERSION = '0.1'

default_db = 'collection'
DEFAULT_DB_HOST = 'localhost'
DEFAULT_DB_PORT = '3306'
DEFAULT_DB_USER = 'root'
DEFAULT_DB_PASSWD = ''

def print_timing(func):
    """
    Time a function using time.time() and the a @ function decorator
    """
    def wrapper(*arg,**keywords):
        t1 = time.time()
        res = func(*arg,**keywords)
        t2 = time.time()
        delta_s = (t2-t1)
        print '* Excution of "%s" took %0.3fms (%0.3fs, %0.3fmin)' \
            % (func.func_name, delta_s*1000.0, delta_s, delta_s/60 )
        return res
    return wrapper


def get_args():
    """
    Command argument parser

    Returns structure:
        args.host    - server host name
        args.port    - server port
        args.db      - DB name
        args.user    - DB user name
        args.passwd  - user password
        args.input   - XML file name
        args.archive - archive processed files
        args.verbose - prints SQL query

    """

    parser = argparse.ArgumentParser(description='Itinerary XML file processing',
                                     conflict_handler='resolve')

    parser.add_argument('--host', '-h', help='Connect to host', default=DEFAULT_DB_HOST)
    parser.add_argument('-P', '--port', help='Server communication port', default=DEFAULT_DB_PORT)
    parser.add_argument('-d', '--db', help='Database (default: collection)', default=default_db)
    parser.add_argument('-u', '--user', help='DB User', default=DEFAULT_DB_USER)
    parser.add_argument('-p', '--passwd', help='User password', default=DEFAULT_DB_PASSWD)
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                         help='Prints out SQL queries sent to the DB (defaul: false)')
    parser.add_argument('-a', '--archive', action='store_true', default=False,
                         help='Archive itinerary XML files after processing (defaul: false)')
    parser.add_argument('--version', action='version', version='%(prog)s '+version)
    parser.add_argument('input', help='Itinerary XML file' )

    args = parser.parse_args()
    return args

def el_val(element, xpath):
    """
    returns 1st encountered element according the specified XPaht value.

    >>> el_val(ET.fromstring('<r><el1>VAL</el1><el1>val</el1><el2>VAL2</el2></r>'),'el1')
    'VAL'
    >>> el_val(ET.fromstring('<r><el1>VAL</el1><el1>val</el1><el2>VAL2</el2></r>'),'el2')
    'VAL2'
    >>> el_val(ET.fromstring('<r><el1>VAL</el1><el1>val</el1><el2>VAL2</el2></r>'),'el101')

    >>> el_val(ET.fromstring('<r><el1>VAL</el1><el1>val</el1><el3>    </el3></r>'),'e3')

    """
    if element is None:
        return None
    el = element.find(xpath)
    return None if (el is None or el.text is None ) else el.text.strip()


def YYYYMMDD(short_date):
    """
    Transorms DDMONYY or DDMONYYYY format date into ISO format: YYYY-MM-DD
    If the year is absent, current system year

    >>> YYYYMMDD("20JAN")
    '2012-01-20'
    >>> YYYYMMDD("20JAN22")
    '1922-01-20'
    >>> YYYYMMDD("20JAN11")
    '2011-01-20'
    >>> YYYYMMDD("20JAN2022")
    '2022-01-20'
    """
    if short_date is None or len(short_date) < 5 or not(short_date[0:2].isdigit()):
        return None

    if len(short_date) > 7:
        # DDMONYYYY date
        dd  = datetime.strptime(short_date, "%d%b%Y")
    else:
        # DDMONYY or DDMON date
        dd = datetime.strptime(short_date, "%d%b%y" if len(short_date) > 5 else "%d%b")
        yy = datetime.now().year

        # Y2K:
        if dd.year > yy:
            dd = dd.replace(year = dd.year-100)
        # Default year:
        if len(short_date) < 6:
            dd = dd.replace(year = yy)
    return dd.strftime('%Y-%m-%d')

def HHMM(short_time):
    """
    Transorms HHMM, HMMP, HMMA format time into ISO 8601 format time: HH:MM:00

    >>> HHMM("1245")
    '12:45:00'
    >>> HHMM("1236")
    '12:36:00'
    >>> HHMM("1157")
    '11:57:00'
    >>> HHMM("2245")
    '22:45:00'
    >>> HHMM("0145")
    '01:45:00'
    >>> HHMM("0825")
    '08:25:00'
    >>> HHMM("350P")
    '15:50:00'
    >>> HHMM("955A")
    '09:55:00'
    """
    if short_time is None:
        return
    elif short_time.isdigit(): # HHMM format
        mm = short_time[2:4]
        hh = short_time[:2]
    else: # HMMP or HMMA format
        day_part = short_time[-1]
        mm = short_time[1:3]
        hh = short_time[:1]
        if day_part=='A':
            hh = '0'+hh
        else:
            hh = str(int(hh)+12 )

    return hh +':'+('00' if mm=='' else mm)+':00'

def ISO8601(short_timestamp):
    """
    Transorms DDMONYY/HHMM format date into ISO 8601 format timestamp: YYYY-MM-DDTHH:MM:00
    If the year is absent, current system year

    >>> ISO8601("20JAN/1245")
    '2012-01-20T12:45:00'
    >>> ISO8601("20JAN22/1232")
    '1922-01-20T12:32:00'
    >>> ISO8601("20JAN11/1121")
    '2011-01-20T11:21:00'
    >>> ISO8601("20JAN11")
    '2011-01-20'
    >>> ISO8601("20JAN2022/1232")
    '2022-01-20T12:32:00'
    """
    if short_timestamp is None:
        return
    if '/' in short_timestamp:
        date_part, time_part = short_timestamp.split('/')
        date_part = YYYYMMDD(date_part)
        return (date_part if time_part is None or time_part == ''
                 else date_part+'T'+HHMM(time_part) )
    else:
        return YYYYMMDD(short_timestamp)

amount_re = re.compile('[0-9.]+')
def amount(val):
    """
    Extracts from value moneytory amount

    >>> amount("USD11.44")
    '11.44'
    >>> amount("11.44USD")
    '11.44'
    >>> amount("4NT")
    '4'
    """
    res = amount_re.search(val)
    return None if res is None else res.group(0)

class Loader:
    """
    Itinerary XML file parser and loader
    """

    __cur = None
    __cn  = None

    def __init__(
            self,
            host = None,
            port = None,
            db = None,
            user = None,
            passwd = None,
            verbose = False,
            archive = False):
        """
        Constructor
        """

        self.__db = default_db if not (db) else db
        self.__host = DEFAULT_DB_HOST if not(host) else host
        self.__port = DEFAULT_DB_PORT if not(port) else port
        self.__user = DEFAULT_DB_USER if not(user) else user
        self.__passwd = DEFAULT_DB_PASSWD if not(passwd) else passwd

        self.__archive = archive
        self.__verbose = verbose


    def _initDb(self):
        """
        Instantiates DB connection and builds all DB objects
        if they do not exist
        """
        self.__cn = DB.connect(
            host = self.__host,
            user = self.__user,
            passwd = self.__passwd,
            db = self.__db )

    def db(self):
        """
        Lazy load of a single DB connection
        """
        if self.__cn == None:
            self._initDb()
        return self.__cn

    def cur(self):
        """
        Lazy load of a single DB cursor
        """
        if self.__cur == None:
            self.__cur = self.db().cursor()
        return self.__cur

    def db_execute(self, query, *args):
        """
        DB SQL statement execution wrapper function
        if "--dry-run" option set, do nothing
        """
        if args is not None:
            args = self.db().literal(args)
        if self.__verbose:
            print '/*'
            print query
            if args is not None:
                for i in range(len(args)):
                    val = args[i]
                    print "{}: {}".format(i, 'NULL' if val is None else val)
            print "*/"
        if args is not None:
            #if not isinstance(args,(list, tuple)):
            #    args = (args,)
            query = query.format(*args )
        if self.__verbose:
            print query
        self.cur().execute(query )
        return self.__cur

    def get_trip_raw(self, record_locator):
        """
        Finds trip row based on record_locator
        Returns:
            tuple (aia_id, customer_no )
        """
        row = self.getone(
            "SELECT aia_id, customer_no FROM trip_raw WHERE record_locator={} ORDER BY aia_id DESC LIMIT 1",
            record_locator )
        if row is None:
            return (None,None)
        else:
            return row

    def getone(self, query, *args ):
        """
        Fecho first query result set row.
        Returns:
            tuple representing one row or None if none retrieved
        """
        cur = self.db_execute(query, *args )
        if not(cur):
            return None
        else:
            row = cur.fetchone()
        if row is None:
            return None
        else:
            return row

    def getscalar(self, query, *args ):
        """
        Fecho the first column value of the first query result set row.
        Returns:
            scalar value or None
        """
        row = self.getone(query, *args)
        return None if row is None else row[0]

    def get_trip_raw_id(self, record_locator):
        """
        Finds trip ID (aia_id) based on record_locator
        """
        return self.getscalar(
            "SELECT aia_id FROM trip_raw WHERE record_locator={} ORDER BY aia_id DESC LIMIT 1", record_locator )

    def reset(self):
        """
        Reset parser in order to parse subsequent documents
        """
        self.close()
        self._init()


    def close(self):
        """
        Close all resourece used for parsing
        """
        self.db().close()
        del self.__cn
        del self.__doc
        del self.__tree

    def segment_start_date(self, seg):
        if seg.tag == 'flight_segments':
            start_date = el_val(seg,'departure_date')
        elif seg.tag == 'car_segments':
            start_date = el_val(seg,'pick_up_date')
        elif seg.tag == 'hotel_segments':
            start_date = el_val(seg,'check_in_date')
        else:
            start_date = None
        return YYYYMMDD(start_date)


    def segment_end_date(self, seg):
        if seg.tag == 'flight_segments':
            org_end_date = el_val(seg,'departure_date')
        elif seg.tag == 'car_segments':
            org_end_date = el_val(seg,'drop_off_date')
        elif seg.tag == 'hotel_segments':
            org_end_date = el_val(seg,'check_out_date')
        else:
            start_date = None
        end_date = YYYYMMDD(org_end_date)
        if seg.tag == 'flight_segments' and el_val(seg,'arrival_time') < el_val(seg,'departure_time'):
            end_date = datetime.strptime(end_date, '%Y-%m-%d')
            end_date = end_date.replace(day=end_date.day+1).strftime('%Y-%m-%d')
        return end_date


    def parse_series(self, file_name):
        """
        Parse and load series XML file
        """
        self.__tree = tree = ET.parse(file_name)
        self.__doc = doc = tree.getroot()
        record_locator = el_val(doc, 'record_locator')
        customer_dk = el_val(doc, 'client_info/customer_dk')
        prev_aia_id = self.get_trip_raw_id(record_locator)
        is_update = (prev_aia_id is not None)
        self.db_execute(
            'INSERT INTO trip_raw (record_locator, customer_no) VALUES ({},{})',
            record_locator, customer_dk )
        aia_id = self.get_trip_raw_id(record_locator)

        # Update Profile data:
        taxi_id = el_val(doc, 'psgr_info/taxi_id')
        if taxi_id is None:
            taxi_id = self.getscalar("SELECT CONCAT('ZZ',LEFT(UPPER(MD5(CURRENT_TIMESTAMP)),8));")
        self.db_execute(
            "INSERT IGNORE INTO user_data "
            "(customer_dk, taxi_id, profile_lock) "
            "VALUES ({},{},2) ",
            customer_dk, taxi_id )
        user_id, profile_lock = self.getone(
            "SELECT id, profile_lock FROM user_data WHERE taxi_id={}", taxi_id)
        if profile_lock==2:
            self.db_execute(
                "INSERT INTO travelers "
                "SET first_name={0}, last_name={1}, date_of_birth={2}, add_psgr={3},"
                "email_1={4}, email_2={5}, email_3={6}, email_4={7}, user_id={8} "
                "ON DUPLICATE KEY UPDATE "
                "first_name={0}, last_name={1}, date_of_birth={2}, add_psgr={3},"
                "email_1={4}, email_2={5}, email_3={6}, email_4={7}",
                el_val(doc, 'psgr_info/firstname'),
                el_val(doc, 'psgr_info/lastname'),
                el_val(doc, 'psgr_info/birthdate'),
                el_val(doc, 'psgr_info/add_psgr'),
                el_val(doc, 'contact_info/trip_email1'),
                el_val(doc, 'contact_info/trip_email2'),
                el_val(doc, 'contact_info/trip_email3'),
                el_val(doc, 'contact_info/trip_email4'),
                user_id )

        traveler_id = self.getscalar("SELECT traveler_id FROM travelers WHERE user_id={}", user_id)

        segments = sorted(doc.findall('itinerary_segments/*'), key = lambda seg: int(el_val(seg, 'seg_no')) )
        if segments is not None and len(segments) > 0:
            org_start_date = self.segment_start_date(segments[0])
            org_end_date = self.segment_end_date(segments[-1])
        else:
            org_start_date = org_end_date = None

        self.db_execute(
            "INSERT INTO trips "
            "SET record_locator={0}, create_date={1}, create_time={2},"
            "org_start_date={3}, org_end_date={4}, booking_agt={5}, depart_city={6},"
            "itin_type={7}, jira_case={8}, source_pcc={9}, traveler_id={10},"
            "home_pcc={11}, customer_dk={12},status_id='ACTIVE' "
            "ON DUPLICATE KEY UPDATE "
            "record_locator={0}, create_date={1}, create_time={2},"
            "org_start_date=COALESCE({3},org_start_date), org_end_date=COALESCE({4},org_end_date),"
            "booking_agt={5}, depart_city={6},itin_type={7}, jira_case={8},"
            "source_pcc={9}, traveler_id={10},home_pcc={11}, customer_dk={12}, mod_date=CURDATE(),"
            "status_id=IF({13}=0,'CANCELLED','UPDATED');",

            record_locator,
            YYYYMMDD(el_val(doc, 'create_date')), #1
            HHMM(el_val(doc, 'create_time')),     #2
            org_start_date,                         #3
            org_end_date,                           #4
            el_val(doc, 'create_agent'),           #5
            el_val(doc, 'itinerary_segments/flight_segments/departure_city_code'),
            el_val(doc, 'itinerary_type'),         #7
            el_val(doc, 'jira_case_no'),           #8
            el_val(doc, 'client_info/source_pcc'), #9
            traveler_id,                            #10
            el_val(doc, 'client_info/home_pcc'),   #11
            customer_dk,                            #12
            len(segments)                          #13
        )

        trip_id = self.getscalar(
            "SELECT trip_id FROM trips WHERE record_locator={} LIMIT 1;",record_locator)


        # Mark absent in XML segments as canceled, if the trip gets updated
        # and the rest - 'INACTIVE'
        if is_update:
            # Segment seg_no list in the XML file:
            seg_no_list = ','.join(el_val(seg, 'seg_no') for seg in segments )
            # Mark 'CANCELED', if seg_no absent in the XML file:
            for segment_type_table_name in ['air_segments', 'hotel_segments', 'car_segments']:
                self.db_execute(
                    "UPDATE " + segment_type_table_name + \
                    "  SET status_id=IF(seg_no IN ("+seg_no_list+"), 'INACTIVE', 'CANCELLED') "
                    "WHERE trip_id={0}", trip_id )

        for seg in segments:
            seg_no = el_val(seg, 'seg_no')
            seg_start_date = self.segment_start_date(seg)
            seg_end_date = self.segment_end_date(seg)

            if seg.tag == 'flight_segments':
                self.db_execute(
                    "INSERT INTO air_segments "
                    "SET trip_id={0}, record_locator={1}, seg_no={2}, airline_code={3}, "
                    "flight_no={4}, class_service={5}, depart_date={6}, arrive_date={7}, "
                    "depart_time={8}, arrive_time={9}, day_of_week_cd={10}, arrive_city={11}, "
                    "source_rec_loc={12}, segment_status={13}, seat_assign={14},"
                    "depart_city={15}, status_id='ACTIVE';",

                    trip_id,
                    record_locator,
                    seg_no,
                    el_val(seg, 'airline'),               #3
                    el_val(seg, 'flight_no'),             #4
                    el_val(seg, 'class_of_service'),      #5
                    seg_start_date,                        #6
                    seg_end_date,                          #7
                    HHMM(el_val(seg, 'departure_time')), #8
                    HHMM(el_val(seg, 'arrival_time')),   #9
                    el_val(seg, 'day_of_week'),           #10
                    el_val(seg, 'arrival_city_code'),     #11
                    el_val(seg, 'source_record_locator'), #12
                    el_val(seg, 'segment_status_code'),   #13
                    el_val(seg, 'seat_no'),               #14
                    el_val(seg, 'departure_city_code')    #15
                )

            elif seg.tag == 'hotel_segments':
                self.db_execute(
                    "INSERT INTO hotel_segments "
                    "SET trip_id={0}, record_locator={1}, seg_no={2},"
                    "hotel_type={3}, chain_code={4}, segment_status={5}, city_code={6}, check_in={7},"
                    "check_out={8}, property_code={9}, rate={10}, cxl_policy={11}, conf_no={12},"
                    "no_nights={13},comm={14},status_id='ACTIVE';",

                    trip_id,
                    record_locator,
                    seg_no,
                    el_val(seg, 'hotel_type'),               #3
                    el_val(seg, 'hotel_vendor_code'),        #4
                    el_val(seg, 'hotel_status_code'),        #5
                    el_val(seg, 'hotel_city_code'),          #6
                    seg_start_date,                           #7
                    seg_end_date,                             #8
                    el_val(seg, 'hotel_sabre_code'),         #9
                    amount(el_val(seg, 'hotel_rate')),      #10
                    el_val(seg, 'cancel_policy'),            #11
                    el_val(seg, 'confirmation_no'),          #12
                    amount(el_val(seg, 'number_of_nights')),#13
                    el_val(seg, 'hotel_commission') )        #14

            elif seg.tag == 'car_segments':
                self.db_execute(
                    "INSERT INTO car_segments "
                    "SET trip_id={0}, record_locator={1}, seg_no={2},"
                    "pickup_date={3}, drop_off_date={4}, car_vendor_cd={5}, day_of_week_cd={6},"
                    "pick_up_time={7}, drop_off_time={8}, daily_rate={9}, total_rate={10},"
                    "conf_no={11}, city_code={12}, seg_status={13}, status_id='ACTIVE';",

                    trip_id,
                    record_locator,
                    seg_no,
                    seg_start_date,
                    seg_end_date,
                    el_val(seg, 'vendor'),                   #5
                    el_val(seg, 'day_of_week'),              #6
                    HHMM(el_val(seg, 'arrival_time')),      #7
                    HHMM(el_val(seg, 'drop_off_time')),     #8
                    amount(el_val(seg, 'car_rate_detail')), #9
                    amount(el_val(seg, 'car_rate_total')),  #10
                    el_val(seg, 'confirmation_no'),          #11
                    el_val(seg, 'pick_up_city_code'),        #12
                    el_val(seg, 'car_seg_status') )          #13


        # Ticketing:
        for ticket in doc.iterfind('ticketing/ticket'):
            ticket_type = el_val(ticket, 'ticket_type')
            if ticket_type!='TE':
                continue
            ticket_no = el_val(ticket, 'ticket_no')
            self.db_execute(
                "INSERT IGNORE INTO etrs "
                "SET trip_id={0}, record_locator={1},"
                "etr_no={2}, invoice_no={3}, arc_no={4}, total_fare={5}, base_fare={6},"
                "linear_fare={7}, tour_code={8}, pcc={9}, name_ref={10}, etr_name={11},"
                "ticket_type={12}, issue_date={13}, endorsement={14};",

                trip_id,
                record_locator,
                ticket_no,
                el_val(ticket, 'etr/invoice'),           #3
                el_val(ticket, 'etr/iata'),              #4
                el_val(ticket, 'etr/etr_total'),         #5
                el_val(ticket, 'etr/base_fare'),         #6
                '\n'.join([e.text.strip() for e in ticket.findall('etr/fare_calc/*') if e.text is not None ]),
                el_val(ticket, 'etr/tour_code'),         #8
                el_val(ticket, 'etr/pcc'),               #9
                el_val(ticket, 'etr/name_ref'),          #10
                el_val(ticket, 'etr/etr_name'),          #11
                el_val(ticket, 'ticket_type'),           #12
                YYYYMMDD(el_val(ticket, 'etr/issued')), #13
                el_val(ticket, 'etr/endorsement'))       #14

            etr_id = self.getscalar(
                'SELECT etr_id FROM etrs WHERE '
                'trip_id={0} AND record_locator={1} AND etr_no={2}',
                trip_id, record_locator, ticket_no)

            # Coupons (ETR segments):
            for coupon in ticket.iterfind('etr/coupons'):
                city_pair_list = [ city_pair.text.strip() for city_pair in coupon.iterfind('city_pair') \
                                   if city_pair.text is not None ]
                self.db_execute(
                    "INSERT INTO etr_segments "
                    "SET etr_id={0}, etr_no={1}, record_locator={2}, coupon_no={3},"
                    "airline={4}, flight_no={5}, class_service={6}, date_depart={7},"
                    "depart_city={8}, arrive_city={9}, dept_time={10}, segment_status={11},"
                    "fare_basis={12}, coupon_status={13} "
                    "ON DUPLICATE KEY UPDATE "
                    "coupon_status={13};",

                    etr_id,
                    ticket_no,
                    record_locator,
                    el_val(coupon, 'coupon_no'),       #3
                    el_val(coupon, 'airline'),         #4
                    el_val(coupon, 'flight'),          #5
                    el_val(coupon, 'class'),           #6
                    YYYYMMDD(el_val(coupon, 'date')), #7
                    ','.join(city_pair_list[:3]),       #8
                    ','.join(city_pair_list[-3:]),      #9
                    HHMM(el_val(coupon, 'time')),     #10
                    el_val(coupon, 'status'),          #11
                    el_val(coupon, 'fare_basis'),      #12
                    el_val(coupon, 'coupon_status'))   #13

        # Pricing:
        pinfo = doc.find('pricing_info')
        if pinfo is not None:

            if is_update:
                # If total
                self.db_execute(
                    "UPDATE pricing SET status_id='INACTIVE' "
                    "WHERE trip_id={0} AND NOT total_fare={1};",
                    trip_id, el_val(pinfo, 'total_fare') )

            rec_exists = self.getscalar(
                "SELECT TRUE FROM pricing WHERE trip_id={0} AND status_id='ACTIVE'", trip_id)

            if not(rec_exists):
                self.db_execute(
                    "INSERT INTO pricing "
                    "(trip_id,record_locator,total_fare,base_fare,total_taxes,fare_type,"
                    "ticket_deadline,create_date,status_id)"
                    "VALUES({0},{1},{2},{3},{4},{5},{6},CURDATE(),'ACTIVE' );",

                    trip_id,
                    record_locator,
                    el_val(pinfo, 'total_fare'),       #2
                    el_val(pinfo, 'base_fare'),        #3
                    el_val(pinfo, 'total_taxes'),      #4
                    el_val(pinfo, 'fare_type'),        #5
                    ISO8601(el_val(pinfo, 'last_day'))#6
                )

            self.db_execute(
                "INSERT INTO fare_comparsion "
                "SET record_locator={0},"
                "lowest_nref_fare={1}, lowest_ref_fare={2},"
                "full_unrestricted_fare={3}, full_premium_fare={4}, first_bag_fee={5},"
                "second_bag_fee={6}, advised_fare={7} "
                "ON DUPLICATE KEY UPDATE "
                "lowest_nref_fare={1}, lowest_ref_fare={2},"
                "full_unrestricted_fare={3}, full_premium_fare={4}, first_bag_fee={5},"
                "second_bag_fee={6}, advised_fare={7};",

                record_locator,
                el_val(pinfo, 'lowest_nref_fare'),       #1
                el_val(pinfo, 'lowest_ref_fare'),        #2
                el_val(pinfo, 'full_unrestricted_fare'), #3
                el_val(pinfo, 'full_premium_fare'),      #4
                el_val(pinfo, 'first_baggage_fee'),      #5
                el_val(pinfo, 'second_baggage_fee'),     #6
                el_val(pinfo, 'advised_fare'))           #7


            udids = [ el.tag+'='+self.db().literal(el.text)
                      for el in pinfo.iter() if el.tag.startswith('udid') if not (el.text is None or el.text.strip()=='' )]
            if udids:
                udids_ex = ','.join(udids )
                sql = \
                "INSERT INTO reporting_fields " \
                "SET trip_id={0}, record_locator={1}," \
                + udids_ex + \
                " ON DUPLICATE KEY UPDATE record_locator={1}," + udids_ex
                self.db_execute(sql, trip_id, record_locator )

        # Form of Payment Handling (FOP):
        ccard = doc.find('credit_cards')
        if ccard is not None:
            self.db_execute(
                "INSERT INTO fop "
                "SET trip_id={0}, record_locator={1},"
                "card_type={2}, card_number={3}, card_exp={4} "
                "ON DUPLICATE KEY UPDATE "
                "card_type={2}, card_number={3}, card_exp={4};",

                trip_id,
                record_locator,
                el_val(ccard, 'card_type'),       #2
                el_val(ccard, 'card_number'),     #3
                el_val(ccard, 'card_expiration')) #4

        # Quality Assurance (QA):
        self.db_execute(
            "INSERT INTO qa "
            "SET trip_id={0}, record_locator={1},"
            "qa_status={2}, date=CURRENT_TIMESTAMP "
            "ON DUPLICATE KEY UPDATE qa_status={2};",

            trip_id,
            record_locator,
            el_val(doc, 'qa_status'))

        # Invoices:
        invoice = doc.find('invoice')
        if invoice is not None:
            invoice_no = el_val(invoice, 'invoice_number')
            invoice_date = YYYYMMDD(el_val(ticket, 'etr/issued'))
            ticket_ex = '\n,'.join([
                "ticketno{0}={1},ticketno{0}amt={2}".format(
                    i,
                    self.db().literal(el_val(el, 'ticket_no')),
                    round(float(el_val(el, 'basefare'))+float(el_val(el, 'total_tax')), 2 ) )
                for i,el in enumerate(invoice.findall('accounting_line[ticket_no]' ),  start=1)] )
            sql = \
            "INSERT INTO invoices " \
            "SET trip_id={0},record_locator={1},invoicenum={2},invoicedate={3},\n" \
            + ticket_ex + \
            "\nON DUPLICATE KEY UPDATE record_locator={1},invoicenum={2},invoicedate={3},\n" + ticket_ex
            self.db_execute(
                sql, trip_id, record_locator, invoice_no, invoice_date )

            payment_sql = \
                "INSERT INTO payments " \
                "SET invoice_no={0},payment_amount={1},payment_method={2},payment_type={3}" \
                "\nON DUPLICATE KEY UPDATE payment_amount={1},payment_method={2},payment_type={3}"
            for payment in invoice.iterfind('payments/*'):
                if payment.tag=='ticket_payment':
                    payment_amount = el_val(payment, 'ticket_payment_amt')
                    payment_type = el_val(payment, 'ticket_payment_type')
                    payment_method = 'Ticket'
                else:
                    payment_amount = el_val(payment, 'servicefee_payment_amt')
                    payment_type = el_val(payment, 'servicefee_payment_type')
                    payment_method = 'ServiceFee'

                self.db_execute(payment_sql, invoice_no,payment_amount,payment_method,payment_type)


            # Service Fees:
            sfees = invoice.find('service_fees')
            if sfees is not None:
                self.db_execute(
                    "INSERT INTO service_fees "
                    "SET trip_id={0}, record_locator={1},"
                    "mco_no={2}, fee_amt={3}, fee_type={4}, invoice_no={5} "
                    "ON DUPLICATE KEY UPDATE "
                    "mco_no={2}, fee_amt={3}, fee_type={4}, invoice_no={5};",

                    trip_id,
                    record_locator,
                    el_val(sfees, 'service_fee_no'),        #2
                    el_val(sfees, 'service_fee_amount'),    #3
                    el_val(sfees, 'service_fee_type'),      #4
                    el_val(sfees, 'associated_invoice_no')) #5

            # Retention Records:
            self.db_execute(
                "INSERT INTO retention_segment "
                "SET trip_id={0}, record_locator={1}, retention_date={2} "
                "ON DUPLICATE KEY UPDATE retention_date={2};",
                trip_id,
                record_locator,
                el_val(sfees, 'sabre_ret_date'))


        # Process statuss:
        if prev_aia_id is not None and (segments is None or len(segments) < 0):
            # all segment are cancelled
            status_id = 'CANCELED'
        elif prev_aia_id is None:
            status_id = 'ACTIVE'
        else:
            status_id = 'UPDATED'
        self.db_execute(
            "INSERT INTO aia_stats "
            "SET aia_id={0},record_locator={1},customer_no={2},traveler_id={3},trip_id={4},"
            "process_time=CURRENT_TIMESTAMP,receive_time=CURRENT_TIMESTAMP,status_id={5};",

            aia_id,         #0
            record_locator, #1
            customer_dk,    #2
            traveler_id,    #3
            trip_id,        #4
            status_id       #5
        )

        self.db().commit()

    def process_itinerary(self, file_name):
        """
        Load parsed itinerary data into DB
        creating a new record or updating an existing one and
        archive the processed file
        """
        self.parse_itinerary(file_name)
        self.close()
        if self.__archive:
            archive_name = os.path.join(
                            os.path.dirname(file_name),
                            '.archive',
                            os.path.basename(file_name))
            os.renames(file_name, archive_name)

def main():
    """
    Invokes parsing and loading processes interactively if the scrip run from terminal
    """
    args = get_args()

    if not os.path.exists(args.input ):
        raise Exception("File or directory %s doesn't exist!" % args.input)

    # data source:
    source = os.path.abspath(args.input)
    # List of XML files:
    source = [source] if os.path.isfile(source) else set(
                        glob.glob(os.path.join(source, '*.xml') )+glob.glob(os.path.join(source, '*.XML') ) )

    loader = ItineraryLoader(
        verbose = args.verbose,
        archive = args.archive,
        host = args.host,
        port = args.port,
        db = args.db,
        user = args.user,
        passwd = args.passwd)

    for file_name in source:
        print "# Processing: %s" % os.path.basename(file_name)
        #loader.process_itinerary(file_name )
        try:
            loader.process_itinerary(file_name )
        except Exception as ex:
            print type(ex)     # the exception instance
            print ex.args      # arguments stored in .args
            print ex           # __str__ allows args to printed directly

if __name__ == '__main__':
    main()
