#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
"""
Created on Thu Aug 18 00:24:44 2016

@author: nad2000
"""


import configparser
from multiprocessing import cpu_count
import sys
import os

DEFAULT_DB = 'mysql'
DEFAULT_DB_HOST = 'localhost'
DEFAULT_DB_PORT = '3306'
DEFAULT_DB_USER = 'root'
DEFAULT_DB_PASSWORD = '12345'

DEFAULT_CATAPULT_USERNAME = "****"
DEFAULT_CATAPULT_USERGROUP = "****"
DEFAULT_CATAPULT_PASSWORD = "****"



try:
    config_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.ini")
    __config = configparser.ConfigParser()
    __config.read(config_file)
    __config = {s.lower() + '_' + k.lower(): v for s in __config.sections() for k,v in __config[s].items()}
except:
    print("Missing or malformated configuration file %r." % config_file)
    sys.exit(-1)

DB_NAME = __config.get("db_name", DEFAULT_DB) 
DB_HOST = __config.get("db_host", DEFAULT_DB_HOST) 
DB_PORT = __config.get("db_port", DEFAULT_DB_PORT) 
DB_USER = __config.get("db_user", DEFAULT_DB_USER) 
DB_PASSWORD = __config.get("db_password", DEFAULT_DB_PASSWORD) 

CATAPULT_USERNAME = __config.get("catapult_username", DEFAULT_CATAPULT_USERNAME) 
CATAPULT_USERGROUP = __config.get("catapult_usergroup", DEFAULT_CATAPULT_USERGROUP) 
CATAPULT_PASSWORD = __config.get("catapult_password", DEFAULT_CATAPULT_PASSWORD) 

