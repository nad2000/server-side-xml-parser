# Server-side-XML-parser

## Original Spec

> # REQUIREMENT / DELIVERABLES
> 
> We require a small, robust server side script and cron job to:
> 
> 1. read a set of instruments and field names from a MySQL database
> 2. connect to the third party webservice, make a data request
> 3. parse the returned XML message into MySQL database tables (or generate an error message)
> 4. run calculations on the updated tables to update additional fields (for example price differences and simple moving standard deivations etc)
> 
> The script must be fully configurable, so that changes to the XML schema/messages and/or calculations can be managed easily, and keep detailed logfiles.  The script language is open to suggestion, but it must be robust, stable and able to run reliably unattended
> 
> The script must also be able to be executed on an adhoc (on demand) basis via a remote request (eg. via a small webservice).v
> 
> The solution will be running on an AWS EC2 linux instance and reading/writing from a MySQL database.
> 
> # EXPERIENCE REQUIRED
> 
> 1. Must have proven experience writing scripts that interface with MySQL databases (insert/delete/update) and webservices.  XML parsing and simple calculations.
> 2. Must have a 100% completion rate on all projects
> 3. Must have a minimum 4.5/5 star rating
> 4. Must have at least 20 hours of work on upwork, or be able to show the equivalent on other freelancer sites
> 5. Knowledge of financial market data (tickers, stock prices) would be useful to have but not required

