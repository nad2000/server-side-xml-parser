#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
#-------------------------------------------------------------------------------
# Name:     loadseries.py
# Author:   Radomirs Cirskis
#
# Created:  2016-08-17
# Licence:  WTFPL
#-------------------------------------------------------------------------------
"""
Created on Wed Aug 17 19:03:43 2016
@author: nad2000

INPUT:
    Sabre Itinerary XML (no name space)
    MySQL DB connection parameters (or via config.ini)

OUTPU:
    New or updated record in DB

STEPS"
    1. parse XML file;
    ....

PREREQUISITES:
    Python 3.x (python3 Ubuntu package)
    MySQL-python (python3-mysql.connector Ubuntu package)
    Requests (python3-request Ubuntu package)
"""
from __future__ import print_function 

from xml.parsers import expat
import re
import sys
import os
from urllib import request
import csv
import mysql.connector as DB
import argparse
import logging
from datetime import datetime, date, time

from config import DB_NAME, DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, \
    CATAPULT_USERNAME, CATAPULT_USERGROUP, CATAPULT_PASSWORD

# Constants:
VERSION = '0.1'

INSERT_DATA_SQL = """INSERT INTO tblData (
    datDatestamp, 
    insID,
    datTicker, 
    datExchange, 
    datExpiry, 
    datStrike,
    datPutCall,
    datIsRawData,
    datIsCOB,
    datClose,
    datCloseChange,
    datHi,
    datLo,
    datHiLoDifference,
    datLast,
    datImpVol,
    datMonthTenor,
    datUser,
    datSD30dClose,
    datSD90dClose,
    datSDStressedClose,
    datSD30dLast,
    datSD90dLast,
    datSDStressedLast)
VALUES (
    %(date)s,   -- datDatestamp
    %(ins_id)s, -- insID
    %(ticker)s, -- datTicker
    NULL, -- datExchange
    NULL, -- datExpiry
    NULL, -- datStrike
    NULL, -- datPutCall
    %(datIsRawData)s, -- datIsRawData
    %(datIsCOB)s, -- datIsCOB
    NULL, -- datClose
    NULL, -- datCloseChange
    %(high)s, -- datHi
    %(low)s, -- datLo
    %(high_low_diff)s, -- datHiLoDifference
    %(last)s, -- datLast
    NULL, -- datImpVol
    NULL, -- datMonthTenor
    NULL, -- datUser
    NULL, -- datSD30dClose
    NULL, -- datSD90dClose
    NULL, -- datSDStressedClose,
    NULL, -- datSD30dLast
    NULL, -- datSD90dLast
    NULL  -- datSDStressedLast  
)
"""

def lazy_property(fn):
    """Decorator that makes a property lazy-evaluated.
    """
    attr_name = '_lazy_' + fn.__name__

    @property
    def _lazy_property(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fn(self))
        return getattr(self, attr_name)
    return _lazy_property


def get_args():
    """
    Command argument parser
    """

    parser = argparse.ArgumentParser(description='Itinerary XML file processing',
                                     conflict_handler='resolve')

    parser.add_argument('--host', '-h', help='Connect to host', default=DB_HOST)
    parser.add_argument('-P', '--port', help='Server communication port', default=DB_PORT)
    parser.add_argument('-d', '--db', help='Database (default: collection)', default=DB_NAME)
    parser.add_argument('-u', '--user', help='DB User', default=DB_USER)
    parser.add_argument('-p', '--password', help='User password', default=DB_PASSWORD)
    parser.add_argument('-S', '--service', action='store_true', default=False,
                         help='User 3r party webservice (defaul: false)')
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                         help='Prints out SQL queries sent to the DB (defaul: false)')
    #parser.add_argument('--debug', action='store_true', default=False,
    #                     help='Debugging w/o actuall data insertion (defaul: false)')
    parser.add_argument('--version', action='version', version='%(prog)s '+VERSION)
    parser.add_argument('input', help='XML file', nargs='?')

    args = parser.parse_args()
    return args

class Loader:
    def __init__(self, 
            user=DB_USER, 
            password=DB_PASSWORD,
            host=DB_HOST,
            database=DB_NAME, 
            port=DB_PORT,
            verbose=False,
            **kwargs):

        self.database = database
        self.db_host = host
        self.db_port = port
        self.db_user = user
        self.db_password = password
        self.verbose = verbose
        self.init_parser()

        # current element tag
        self.tag = None
        self.rec_count = 0

    def init_parser(self):
        self.parser = expat.ParserCreate()
        self.parser.buffer_text = True
        # Buffer 1Mb (default: 8Kb):
        self.parser.buffer_size = 1048576
        self.parser.StartElementHandler = self.start_tag_handler
        self.parser.EndElementHandler = self.end_tag_handler
        self.parser.CharacterDataHandler = self.data_handler


    def reset(self):
        """ Reste parser in order to parse subsequent documents """
        self.close()
        self.__init__()
        
    @lazy_property
    def db(self):
        try:
            db = DB.connect(user=self.db_user, password=self.db_password,
                host=self.db_host, database=self.database)
        except Exception as ex:
            logging.fatal("Failed to connect to DB: %r", ex)
            sys.exit(1)
        return db

    @lazy_property
    def cr(self):
        return self.db.cursor()

    @lazy_property
    def instruments(self):
        """
        All instruments (cacheed)
        """
        cr = self.db.cursor()
        cr.execute("SELECT * FROM tblInstruments")
        data = {r[13]: dict(zip(cr.column_names, r)) for r in cr}
        cr.close()
        return data
        
    def get_quote(self):
        req = request.urlopen("http://*****************/XML/Quote.asp?Symbol=IBM.N,NESN.S,ADS.XE&Fields=Last,Open&username=*****************&usergroup=*****************&password=*****************")
        return req.readall()

    def get_timeseries(self):
        req = request.urlopen("http://*****************/XML/TimeSeries.xml?Symbol=IBM.N&TimeScale=1440&MaxPoints=500&From=20160713&To=20160809&Fields=last,bid,ask,high,low,date&Direction=Forward&username=*****************&usergroup=*****************&password=*****************")
        return req.readall()

    def load_data(self, data):
        if b"&amp;" not in data:
            data = data.replace(b"&", b"&amp;")
            
        self.parser.Parse(data, 0)

    def load_from_file(self, file_name):
        with open(file_name, mode='rb') as f:
            data = f.read()
            self.load_data(data)
            
    def close(self):
        # self.parser.Parse("", 1) # end of data
        del self.parser # get rid of circular references

    def start_tag_handler(self, tag, attrs):
        """
        Open tag "<tag>" handler
        """
        if self.verbose:
            logging.info("<%s> with: %r", tag, attrs)
        
        if tag in ("QUOTELIST", "TIMESERIES"):
            self.date = self.ins_id = self.symbol = self.last = self.open = \
            self.datIsCOB = self.high = self.low = self.ticker = self.status = None
            self.rec_count = 0
        elif tag == "TIMESERIESPOINT":
            self.last = self.bid = self.ask = self.high = self.low = self.date = None
        
        # if <last src="pclose"> exists then datIsCOB = 1:
        if tag == "last":
            self.datIsCOB = 1 if attrs.get("src") == "pclose" else None

        if tag in ("QUOTE", "TIMESERIES", "TIMESERIESPOINT"):
            self.__dict__.update(attrs)
            self.tag = tag
        else:
            self.tag = None

    def end_tag_handler(self, tag):
        """
        Closing tag "</tag>" handler
        """
        if tag in ("TIMESERIESPOINT", "QUOTE"):
            if self.high is None or self.low is None:
                self.high_low_diff = None
            else:
                self.high_low_diff = float(self.high) - float(self.low)
            if tag == "TIMESERIESPOINT":
                pass
            elif tag == "QUOTE":
                pass 
                
            instrument = self.instruments.get(self.ticker or self.symbol)
            if instrument is None:
                logging.error("Missing instrument in DB for: %s.", self.ticker or self.symbol)
                self.ins_id = None
                return
            else:
                self.ins_id = instrument["insID"]
                self.datIsRawData = 1
                
            try:
                print(self.datIsCOB)
                self.cr.execute(
                    INSERT_DATA_SQL,
                    {a:v for a, v in self.__dict__.items() if not a.startswith("_")
                        and (v is None or isinstance(v, (str, int, float)))}
                )
            except Exception as ex:
                logging.error("Executing %s:\n%r", self.cr.statement, ex)
        # list of elements that should be imported        
        elif tag in ("last", "open", "bid", "high", "ask", "low", "date"):
            self.__dict__[tag] = self.data
        elif tag == "INFOTECXML":
            self.db.commit()
            logging.info("Processed %d records", self.rec_count)

    def data_handler(self, data):
        """
        Element data handler
        """
        self.data = data

    def print_value_line(self, line):
        """
        print INSERT statement value line and keep track of the processed records
        """
        if self.rec_count != 0:
            print(",\n",)
        sys.stdout.write(line)
        self.rec_count += 1

def main():

    args = get_args()
    if args.verbose:
        print(args)

    l = Loader(**args.__dict__)
    file_name = args.input
    if file_name is None or not(os.path.exists(file_name)) or args.service:
        quotes = l.get_quotes()
        l.load_data(quotes)
        timeseries = l.get_timeseries()
        l.load_data(timeseries)
    else:
        l.load_from_file(file_name)
    l.close()
    pass

if __name__ == '__main__':
    main()
